#!/bin/sh

check_server() {
    HOST=$1
    echo "Checking for liviness: " $1
    while true 
    do
        status=`mongo --eval "getMemInfo()" $HOST | egrep "object|virtual"`

        if [ -z "$status" ]; then
            echo "$HOST is down. Sleeping 10 secs."
            sleep 10
        else
            echo "$HOST is up."
            break
        fi
    done
}

setup_config() {
    cat <<EOF
EOF > /etc/mongod.conf
systemLog:
    destination: file
    path: "$DATADIR/log/mongod.log"
    logAppend: true
processManagement:
    fork: true
    pidFilePath: "$DATADIR/pidfile"
storage:
    dbPath: "$DATADIR"
replication:
    replSetName: "default"
EOF
}

start_replication() {
    cat <<EOF
EOF > /var/log/replicaSetConfigInit.js
var config = {_id: "default", members : [
{_id : 0, host:"$PHOST:27017", priority:2},
{_id : 1, host:"$SHOST1:27017",priority:1},
{_id : 2, host:"$SHOST2:27017",arbiterOnly:true}
]};
var err = rs.initiate(config);
printjson(err);
EOF

    /usr/bin/mongo /var/log/replicaSetConfigInit.js > /var/log/replica-setup.log 2>&1
}

#
# Setup default parameters.
#

if [ -z "$DATADIR" ]; then
    DATA_DIR=/data
fi

if [ -z "$GET_HOSTS_FROM" ]; then
    GET_HOSTS_FROM=ENV
fi

if [ "$GET_HOSTS_FROM" = "DNS" ]; then
    PHOST=mongo-p
    SHOST1=mongo-s1
    SHOST2=mongo-s2
fi

#
# Make sure storage is ready.
#

mkdir -p $DATADIR
mkdir -p $DATADIR/db
mkdir -p $DATADIR/log

#
# Start MongoDB.
#
echo "Starting mongod."
setup_config
/usr/bin/mongod --config /etc/mongod.conf > /var/log/mongod-start.log 2>&1

#
# For primary, we need to wait until all secondaries are up before
# we start replication.
#
if [ "$ROLE" = "PHOST" ]; then
    check_server $SHOST1
    check_server $SHOST2

    start_replication
fi

echo "Ready."
while true
do
    sleep 60
done
